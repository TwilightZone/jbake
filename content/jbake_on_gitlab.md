title=JBake on GitLab
date=2017-02-10
type=post
tags=JBake, GitLab
status=published
~~~~~~
Welcome!  
This is a website that is baked with [JBake](http://jbake.org) and hosted on [GitLab Pages](https://pages.gitlab.io).  
It uses the [default Freemarker templates](https://github.com/jbake-org/jbake-example-project-freemarker) of JBake.  
For more detail, go to <https://gitlab.com/pages/jbake>.